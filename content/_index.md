## Welcome to the fogSLAAntillas website.

As Cloud, Edge and Network infrastructures grow, so does the domestic and industrial digital use. To cope with users demands, infrastructures and services providers rely on scheduling, fragmentation and virtualization techniques to dynamically adapt resource allocation over servers and networks.

At large scales, in distributed contexts or within CPSoS operations, understanding systems operations and adapt them in realtime to users needs or business goals is complex. Solutions variety, private APIs and users specifics limit the ability to centralize and distribute scheduling oriented information.

We propose through fogSLAAntillas an end-to-end implementation of open source and custom developments to demonstrate the ability to schedule large scale distributed systems through high level objectives and multi-dimensional constraints, with the support of rich dynamic SLAs definition.

fogSLAAntillas is a collaborative French project financed by the BPI through the "AMI Cloud" R&D funding program. fogSLAAntillas is also the French counterpart of the European initiative "AiNet/Antillas", supported by Celtic-Next, along with Finland and Germany.
