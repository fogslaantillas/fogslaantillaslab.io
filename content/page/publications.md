# Publications

## 2024

### Conference papers

#### SLA dynamiques et Fog computing ; COMPAS 2024, Nantes
*Amaury Sauret, Christophe Cérin, Khaled Boussetta, Gladys Diaz*

#### Tactical Orchestration - Network, Security, and Drone Intelligence for Mission-Critical Operations; 2024 EuCNC & 6G Summit
*Jani Suomalainen, Kimmo Ahola, Mirko Sailio, Gabor Kiss, Gabor Megyaszai, Rizwan Asif, Petri Jehkonen, Jonathan Rivalan*

#### KOptim: Kubernetes Optimization Framework; 2024 IEEE International Parallel and Distributed Processing Symposium Workshops (IPDPSW), San Francisco, USA (https://pdco2024.sciencesconf.org/resource/page/id/4)
*Tarek Menouer, Christophe Cérin, Patrice Darmon*

#### HOTS : A containers resource allocation hybrid method using machine learning and optimization; 2024 OLA International Conference on Optimization and Learning (sciencesconf.org:ola2024:515403)
*Etienne Leclercq, Jonathan Rivalan, Céline Rouveirol, Frédéric Roupin*

#### HOTS : Allocation de ressources pour conteneurs par une méthode hybride machine learning - optimisation; 2024 GDR RO : Roadef
*Etienne Leclercq, Jonathan Rivalan, Céline Rouveirol, Frédéric Roupin*

### Workshops

#### CO2 Biller : des métriques systèmes à l'équivalence carbone; 2024 GDR RDE : Greendays Toulouse
*Jonathan Rivalan*


## 2023

### Conference papers

#### The EcoIndex metric, reviewed from the perspective of Data Science techniques; 2023 IEEE 47th Annual Computers, Software, and Applications Conference (COMPSAC), Torino, Italy, 2023, pp. 1141-1146, doi: 10.1109/COMPSAC57700.2023.00172 (https://ieeexplore.ieee.org/document/10196912)
*Christophe Cérin, Denis Trystram and Tarek Menouer*

#### Relaxing dispersion pre-distortion constraints of receiver-based power profile estimators; 2023 IEEE OECC Opto-Electronics and Communications Conference
*Louis Tomczyk, Elie Awwad, Petros Ramantanis, Cedric Ware*

####  Exploring code2vec and ASTminer for Python Code Embeddings; 2023 IEEE 3rd International Conference on Software Engineering and Artificial Intelligence
*Long H. Ngo, Veeraraghavan Sekar, Etienne Leclercq, Jonathan Rivalan*
